import { useRef, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import "../App.css";

function Login() {
  const userRef = useRef();

  const [user, setUser] = useState("");
  const [pwd, setPwd] = useState("");

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    sessionStorage.clear();
  });

  const navigate = useNavigate();

  const handleLoginSubmit = (e) => {
    e.preventDefault();

    if (validate()) {
      fetch("https://dummyjson.com/auth/login", {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify({
          username: user,
          password: pwd,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((response) => {
          if (user !== "kminchelle") {
            toast.error("Please Enter valid username");
          } else {
            if (pwd === "0lelplR") {
              toast.success("Sucesfully Logged In");
              sessionStorage.setItem("user", user);
              sessionStorage.setItem("jwtToken", response.token);
              navigate("/home");
            } else {
              toast.error("Please Enter valid credentials");
            }
          }
        })
        .catch((err) => {
          toast.error("Login Failed due to : " + err.message);
        });
    }
  };

  /*
  const handleLoginSubmit = (e) => {
    e.preventDefault();
        if (validate()) {
            ///implentation
            // console.log('proceed');
            fetch("http://localhost:3000/users/" + user).then((res) => {
                return res.json();
            }).then((resp) => {
                console.log(resp)
                if (Object.keys(resp).length === 0) {
                    toast.error('Please Enter valid username');
                } else {
                    if (resp.pwd === pwd) {
                        toast.success('Success');
                        sessionStorage.setItem('username',user);
                        navigate('/')
                    }else{
                        toast.error('Please Enter valid credentials');
                    }
                }
            }).catch((err) => {
                toast.error('Login Failed due to :' + err.message);
            });
        }
  };
  */

  const validate = () => {
    let res = true;
    if (user === "" || user === null) {
      res = false;
      toast.warning("Please Enter User Name");
    }
    if (pwd === "" || pwd === null) {
      res = false;
      toast.warning("Please Enter Password");
    }
    return res;
  };

  return (
    <>
      <section>
        <h1>Sign In</h1>
        <form onSubmit={handleLoginSubmit}>
          <label>User Name</label>
          <input
            type="text"
            id="user"
            ref={userRef}
            onChange={(e) => setUser(e.target.value)}
            value={user}
          />
          <label>Password</label>
          <input
            type="password"
            id="password"
            onChange={(e) => setPwd(e.target.value)}
            value={pwd}
          />
          <button>Sign In</button>
        </form>
        <p>
          Don't an account? <br />
          <span className="line">
            <a href="#">Sign Up </a>
          </span>
        </p>
      </section>
    </>
  );
}

export default Login;
