import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function Home() {
  const navigate = useNavigate();
  const [data, setData] = useState(null);

  useEffect(() => {
    let user = sessionStorage.getItem("user");
    let jwtToken = sessionStorage.getItem("jwtToken");

    if (user === "" || user === null) {
      navigate("/login");
    }

    fetch("https://dummyjson.com/products", {
      headers: {
        Authorization: "bearer " + jwtToken,
      },
    })
      .then((res) => {
        return res.json();
      })
      .then((response) => {
        setData(response);
      })
      .catch((err) => {});
  }, []);

  return (
    <div className="App">
      Home Component
      <div>
        <button
          onClick={() => {
            navigate("/login");
          }}
        >
          Logout
        </button>
        {console.log(data)}
      </div>
    </div>
  );
}
export default Home;
