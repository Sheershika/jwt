import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home"


function App() {
  return (
    <div className="App">
      <ToastContainer theme="colored"></ToastContainer>
      <BrowserRouter>
        <Routes>
            <Route path = '/' element = {<Register />}/>
            <Route path = '/login' element = {<Login />}/>
            <Route path = '/home' element = {<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
